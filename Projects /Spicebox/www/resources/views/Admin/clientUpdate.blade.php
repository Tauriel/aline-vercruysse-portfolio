@extends('home')
@section('content')
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title">Update client (ID: {{$client[0]->pk_id_client}})</div>
        </div>
        <div class="panel-body detail">
            <form method="POST" action="/admin/client/{{$client[0]->pk_id_client}}/do_update" class="form-horizontal"
                  enctype="multipart/form-data" role="form">
                {!! csrf_field() !!}

                <label class="col-md-1 control-label text-center" for="name">Name</label>
                <div class="col-md-11">
                    <input id="name" name="name" type="text" placeholder= "Name" class="form-control input-md"
                           value="{{$client[0]->name}}">
                </div>

                <label class="col-md-1 control-label text-center" for="email">Email</label>
                <div class="col-md-11">
                    <input id="email" name="email" type="text" placeholder="Email" class="form-control input-md"
                            value="{{$client[0]->email}}">
                </div>

                <label class="col-md-1 control-label text-center" for="pass">Password</label>
                <div class="col-md-11">
                    <input id="pass" name="pass" type="text" placeholder="Password" class="form-control input-md"
                           value="{{$client[0]->password}}">
                </div>

                <label class="col-md-1 control-label text-center" for="street">Street</label>
                <div class="col-md-11">
                    <input id="street" name="street" type="text" placeholder="Street" class="form-control input-md"
                           value="{{$client[0]->street}}">
                </div>

                <label class="col-md-1 control-label text-center" for="number">Number</label>
                <div class="col-md-11">
                    <input id="number" name="number" type="text" placeholder="number" class="form-control input-md"
                           value="{{$client[0]->number}}">
                </div>

                <label class="col-md-1 control-label text-center" for="pCode">Postal code</label>
                <div class="col-md-11">
                    <input id="pCode" name="pCode" type="text" placeholder="Postal Code" class="form-control input-md"
                           value="{{$client[0]->postal_code}}">
                </div>

                <label class="col-md-1 control-label text-center" for="pCode">City</label>
                <div class="col-md-11">
                    <input id="city" name="city" type="text" placeholder="City" class="form-control input-md"
                           value="{{$client[0]->city}}">
                </div>

                <label class="col-md-1 control-label text-center" for="pCode">Country</label>
                <div class="col-md-11">
                    <input id="country" name="country" type="text" placeholder="Country" class="form-control input-md"
                           value="{{$client[0]->country}}">
                </div>


                <div class="col-md-12 text-center">
                    <button id="submit" name="submit" class="btn btn-success ">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection