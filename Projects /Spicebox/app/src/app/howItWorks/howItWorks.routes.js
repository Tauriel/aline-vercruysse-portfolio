;(function () {
    'use strict';

    angular.module('app.howItWorks').config(Routes);

    /* @ngInject */
    function Routes($stateProvider) {
        $stateProvider.state('howItWorks', {
            cache: false, // false will reload on every visit.
            templateUrl: 'html/howItWorks/howItWorks.view.html',
            url: '/howItWorks'
        });
    }

})();
