<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    protected $primaryKey = "pk_id_category";

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function products (){
        return $this -> HasMany(Product::class);
    }
}
