;(function () {
    'use strict';

    angular.module('app.home')
        .directive('homePage', function() {

        return {
            restrict: 'E',
            templateUrl: 'html/home/home.view.html'
        };
    });

})();