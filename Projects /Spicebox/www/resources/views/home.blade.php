@extends('layouts/app')

@section('content')

    <body class="skin-red">
    <div class="wrapper">

        <!-- Header -->
        @include('header')
                <!-- Sidebar -->
        @include('sidebar')

            <!-- Main content -->
            <section class="content">
                <!-- Your Page Content Here -->
                @yield('content')
            </section><!-- /.content -->
        </div><!-- /.content-wrapper -->
        <!-- Footer -->


@stop
