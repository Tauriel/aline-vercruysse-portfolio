<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{


    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('pk_id_category');

            $table->string('category_name')->unique();
            $table->text('category_description');

            $table->timestamps();
            $table->softDeletes();
        });

    }

    public function down()
    {
        Schema::drop('categories');
    }
}
