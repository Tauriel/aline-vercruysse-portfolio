;(function () {
    'use strict';

    angular.module('app.client')
        .controller('ClientController', ClientController);

    /* @ngInject */
    function ClientController($log, $state, ClientResource, LoggedInClientResource, AddressResource, OrderResource) {
        $log.info(ClientController.name, Date.now());
        $log.log('Current state name:', $state.current.name);

        var vm = this;

        vm.$$ix = {};
        vm.$$ui = {};

        vm.client = new ClientResource();
        vm.loggedInClient = LoggedInClientResource;
        console.log("Logged in client: " + vm.loggedInClient);

        switch ($state.current.name) {
            //Both register and login
                //at register form
            case 'client.prelogin': {
                vm.$$ix.save = createClientAction;//user hits register -> go go createClientAction
                vm.$$ui.title = 'Prelogin';
                vm.loginClient = loginClient; //user hits login -> go to loginUser
                break;
            }
            //view profile
            case 'client.profile':
                vm.$$ui.title = 'Profile';
                vm.$$ix.save = UpdateClientInfoAction;
                vm.orders = getOrdersAction();
                break;
            default:
                break;
        }

        function rememberClient(resource) {
            LoggedInClientResource.info.name = resource['name'];
            LoggedInClientResource.info.email = resource['email'];
            LoggedInClientResource.info.pk_id_client = resource['pk_id_client'];
            LoggedInClientResource.address.pk_id_address = resource['pk_id_address'];
            LoggedInClientResource.address.street = resource['street'];
            LoggedInClientResource.address.number = resource['number'];
            LoggedInClientResource.address.postal_code = resource['postal_code'];
            LoggedInClientResource.address.city = resource['city'];
            LoggedInClientResource.address.country = resource['country'];

            console.log("Remembering: " + LoggedInClientResource);
        }

        //user hits login -> check if user exists -> get client data
        function loginClient() {
            vm.client.clientRequest = true; //client related
            vm.client.isNew = false; //not new user
            $log.info(loginClient.name, 'client:', vm.client);

            //required 'hack' to circumvent CORS
            //creating and logging in through same backend function 'store'
            //vm.client.isNew = false; -> backend knows to not create new user but return existing user
            vm.client.$save(success, error);

            function error(error) {
                $log.error(loginClient.name, 'ERROR', 'error:', error);
            }

            //on succesful $save -> log ->
            //save client data from backend in a service for access across all controllers and states
            //in case of refresh/redirect, still have access to client data
            function success(resource, responseHeader) {
                $log.log(loginClient.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                rememberClient(resource);

                $state.go('client.profile', {}, { reload: true });
            }
        }

        //create new client
        function createClientAction() {
            vm.client.clientRequest = true; //client related
            vm.client.isNew = true;
            $log.info(createClientAction.name, 'client:', vm.client);

            //go to backend controller -> post
            vm.client.$save(success, error);

            function error(error) {
                $log.error(createClientAction.name, 'ERROR', 'error:', error);
            }

            //on success log and go to client profile
            function success(resource, responseHeader) {
                $log.log(createClientAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                rememberClient(resource);
                $state.go('client.profile', {}, { reload: true });
            }
        }

        //update user info and user can add address
        function UpdateClientInfoAction() {
            vm.client.clientRequest = false; //address related
            $log.info(UpdateClientInfoAction.name, 'client:', vm.loggedInClient);

            //go to backend controller -> post
            vm.client.street = vm.loggedInClient.address.street;
            vm.client.number = vm.loggedInClient.address.number;
            vm.client.postal_code = vm.loggedInClient.address.postal_code;
            vm.client.city = vm.loggedInClient.address.city;
            vm.client.country = vm.loggedInClient.address.country;
            vm.client.id_client = vm.loggedInClient.info.pk_id_client;

            if(LoggedInClientResource.address.pk_id_address === null) {
                vm.client.isNew = true;
                console.log("Saving address: " + vm.client);
                vm.client.$save(success, error);
            }
            else {
                vm.client.pk_id_address = LoggedInClientResource.address.pk_id_address;
                vm.client.isNew = false;
                console.log("Updating address: " + vm.client);
                vm.client.$save(success, error);
            }

            function error(error) {
                $log.error(UpdateClientInfoAction.name, 'ERROR', 'error:', error);
            }

            //on success log and go to client profile
            function success(resource, responseHeader) {
                $log.log(UpdateClientInfoAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                $state.go('client.profile', {}, { reload: true });
            }
        }

        function getOrdersAction() {
            $log.log(getOrdersAction.name);

            var params = {
                pk_id_client: $state.params.pk_id_client
            };

            vm.$$ui.loader = true;

            return OrderResource.show(params, success, error);

            function error(error) {
                $log.error(getOrdersAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getOrdersAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                $log.info("This is response: " + resource[0]);
                vm.$$ui.loader = false;
            }
        }
    }

})();
