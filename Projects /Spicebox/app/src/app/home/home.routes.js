;(function () {
    'use strict';

    angular.module('app.home').config(Routes);

    /* @ngInject */
    function Routes($stateProvider) {
        $stateProvider.state('home', {
                cache: false, // false will reload on every visit.
                templateUrl: 'html/home/home.view.html',
                url: '/'
            });
    }

})();