<?php


namespace App\Http\Controllers\Api;

use Request;
use App\Http\Requests;
use App\Models\Product;
use App\Models\Category;
use App\Models\Btw;
use Log;
use App\Http\Controllers\Controller;

class ApiProductsController extends Controller
{

    //get all products
    public function index(){
        $products = Product::join('btws', 'products.id_btw', '=', 'btws.pk_id_btw')
            ->join('categories', 'products.id_category', '=', 'categories.pk_id_category')
            ->get();


        return $products ?: response()
            ->json([
                'error' => "No products found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }

    //get 1 product
    public function show ($id){
        $product = Product::join('btws', 'products.id_btw', '=', 'btws.pk_id_btw')
            ->join('categories', 'products.id_category', '=', 'categories.pk_id_category')
            ->where('products.pk_id_product', ($id))
            ->get();

        return $product;
    }


}

