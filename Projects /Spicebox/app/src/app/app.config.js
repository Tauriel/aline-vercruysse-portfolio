/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app')
        .config(Config);

    /* @ngInject */
    function Config($mdThemingProvider, $mdIconProvider) {
        $mdThemingProvider.definePalette('spiceboxThemeRed', {
            '50': '#e16c6c',
            '100': '#dc5757',
            '200': '#d84141',
            '300': '#d42c2c',
            '400': '#bf2727',
            '500': '#aa2323',
            '600': '#951f1f',
            '700': '#801a1a',
            '800': '#6b1616',
            '900': '#551212',
            'A100': '#e58181',
            'A200': '#e99696',
            'A400': '#000000',
            'A700': '#ffffff'
        });
        $mdThemingProvider.definePalette('spiceboxThemeYellow', {
            '50': '#fdd89c',
            '100': '#fcce83',
            '200': '#fcc46a',
            '300': '#fbba51',
            '400': '#fbb038',
            '500': '#faa61f',
            '600': '#f99c06',
            '700': '#e18d05',
            '800': '#c87d04',
            '900': '#af6d04',
            'A100': '#fde1b5',
            'A200': '#feebce',
            'A400': '#fef5e7',
            'A700': '#965e03'
        });
        $mdThemingProvider.definePalette('spiceboxThemeBackground', {
            '50': '#ffffff',
            '100': '#ffffff',
            '200': '#ffffff',
            '300': '#f8f5f3',
            '400': '#eee7e4',
            '500': '#e4d9d4',
            '600': '#dacbc4',
            '700': '#d0bdb5',
            '800': '#c6afa5',
            '900': '#bda295',
            'A100': '#ffffff',
            'A200': '#ffffff',
            'A400': '#ffffff',
            'A700': '#000000'
        });
        $mdThemingProvider.theme('default')
            .warnPalette('spiceboxThemeYellow'), {
            'default': '500',
            'hue-1': '400',
            'hue-2': '300',
            'hue-3': '50'
        }
        $mdThemingProvider.theme('default')
            .primaryPalette('spiceboxThemeRed'), {
            'default': '500',
            'hue-1': '400',
            'hue-2': 'A400',
            'hue-3': 'A700'
        }
        $mdThemingProvider.theme('default')
            .backgroundPalette('spiceboxThemeBackground'), {
            'default': '500',
            'hue-1': 'A100',
            'hue-2': 'A700',
            'hue-3': '50'
        }
    }
})();