/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
(gulp => {
    'use strict';

    const CFG = global.CONFIG;

    gulp.task('images', () => {
        gulp.src(`${CFG.dir.src}images/**/*.{gif,ico,jpg,jpeg,png,svg}`)
            .pipe(gulp.dest(`${CFG.dir.dest}images`));
    });

})(require('gulp'));