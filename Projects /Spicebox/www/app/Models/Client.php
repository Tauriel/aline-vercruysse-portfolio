<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    protected $primaryKey = "pk_id_client";
    protected $fillable = ['name', 'email', 'password'];

    use SoftDeletes;

    public function subscriptions (){
        return $this -> hasMany(Subscription::class);
    }
}