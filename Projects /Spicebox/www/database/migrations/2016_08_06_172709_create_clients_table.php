<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{

    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('pk_id_client');
            $table->text('name');
            $table->string('email')->unique();
            $table->string('password', 60);

            $table->timestamps();
            $table->softDeletes();
        });

    }

    public function down()
    {
        Schema::drop('clients');
    }

}
