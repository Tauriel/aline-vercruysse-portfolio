;(function () {
    'use strict';

    angular.module('app', [
        'ngAnimate',
        'ngMaterial',
        'ngMessages',
        'ngResource',

        'ui.router',

        'app.home',
        'app.products',
        'app.productDetail',
        'app.howItWorks',
        'app.services',
        'app.client'
    ]);

    angular.module('app.home', []);
    angular.module('app.products', [])
    angular.module('app.productDetail', []);
    angular.module('app.howItWorks', []);
    angular.module('app.client', []);
    angular.module('app.services', []);

})();