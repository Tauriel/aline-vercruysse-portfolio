<?php

Route::group(['middleware' => 'cors', 'namespace' => 'Api', 'prefix' => 'api'], function () {
    $productOptions =  ['only' => ['index', 'show']];
    $addressOptions =  ['only' => ['store', 'update']];

    Route::resource('products', 'ApiProductsController', $productOptions);
    Route::resource('clients', 'ApiClientsController');
    Route::resource('orders', 'ApiOrdersController');
    Route::resource('addresses', 'ApiAddressController', $addressOptions);
});

