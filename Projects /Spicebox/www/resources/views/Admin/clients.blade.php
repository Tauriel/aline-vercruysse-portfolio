@extends('home')

@section('content')

    <div class="row">
        <h1>Clients</h1>
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <td>Id</td>
                <td>Name</td>
                <td>Email</td>
                </thead>
                <tbody>

            @foreach ($clients as $client)
                <tr>
                    <td>{{$client->pk_id_client}}</td>
                    <td>{{$client->name}}</td>
                    <td>{{$client->email}}</td>

                    <td>
                        <a href="/admin/client/destroy/{{$client->pk_id_client}}"><button class="btn btn-danger" ><i class = "glyphicon glyphicon-remove"></i></button></a>
                        <a href="/admin/client/{{$client->pk_id_client}}"> <button class="btn btn-success "><i class="glyphicon glyphicon-pencil"></i></button></a>
                        <a href="/admin/client/{{$client->pk_id_client}}"><button class="btn btn-info"><i class="glyphicon glyphicon-eye-open"></i> </button></a>
                    </td>
                </tr>
                </tbody>
    @endforeach
            </table>
        </div>
    </div>
@endsection