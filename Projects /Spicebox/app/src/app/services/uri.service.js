/**
 * @author    Olivier Parent
 * @copyright Copyright © 2015-2016 Artevelde University College Ghent
 * @license   Apache License, Version 2.0
 */
;(function () {
    'use strict';

    angular.module('app.services')
        .factory('UriService', UriService);

    /* @ngInject */
    function UriService(
        // Angular
        $location,
        // Custom
        CONFIG
    ) {
        return {
            getApi: getApi
        };

        function getApi(path) {
            var protocol = CONFIG.api.protocol ? CONFIG.api.protocol : $location.protocol(),
                host     = CONFIG.api.host     ? CONFIG.api.host     : $location.host(),
                uri      = protocol + '://' + host + CONFIG.api.path + path;

            return uri;
        }

    }

})();
