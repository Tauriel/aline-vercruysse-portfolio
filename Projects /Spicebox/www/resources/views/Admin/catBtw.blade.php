@extends('home')

@section('content')
    <h1>Categories</h1>

    @foreach ($categories as $category)
    <form method="POST" action='/admin/cats_btws/doCatUpdate/{{$category -> pk_id_category}}' class="form-horizontal"
          enctype="multipart/form-data" role="form">
        {!! csrf_field() !!}
        <label class="col-md-1 control-label text-center" for="name">Name</label>
        <div class="col-md-11 text-center">
            <input id="name" name="name" type="text" placeholder="Category name" class="form-control input-md"
                   required="" value="{{$category->category_name}}">
        </div>

        <label class="col-md-1 control-label text-center" for="desc">Description</label>
        <div class="col-md-11">
            <input id="desc" name="desc" type="text" placeholder="Category description" class="form-control input-md"
                   required="" value="{{$category->category_description}}">
        </div>

        <div class="col-md-12 text-center">
            <button id="submit" name="submit" class="btn btn-success ">Update</button>
            <a href ="/admin/cats_btws/destroyCat/{{$category->pk_id_category}}"><button class="btn btn-danger" ><i class = "glyphicon glyphicon-remove"></i></button></a>
            </br></br></br>
        </div>

    </form>


    @endforeach

    <h1>Add new category</h1>
    <form method="POST" action="/admin/cats_btws/newCat" class="form-horizontal"
          enctype="multipart/form-data" role="form">
        {!! csrf_field() !!}
        <label class="col-md-1 control-label text-center" for="name">Name</label>
        <div class="col-md-11 text-center">
            <input id="name" name="name" type="text" placeholder="Category name" class="form-control input-md" required="">
        </div>

        <label class="col-md-1 control-label text-center" for="desc">Description</label>
        <div class="col-md-11">
            <input id="desc" name="desc" type="text" placeholder="Category description" class="form-control input-md" required="">
        </div>

        <div class="col-md-12 text-center">
            <button id="submit" name="submit" class="btn btn-success ">Add</button>
        </div>
    </form>

    <h1>BTW</h1>
    @foreach ($btws as $btw)
        <form method="POST" action='/admin/cats_btws/doBtwUpdate/{{$btw -> pk_id_btw}}' class="form-horizontal"
              enctype="multipart/form-data" role="form">
            {!! csrf_field() !!}
            <label class="col-md-1 control-label text-center" for="btw">Btw</label>
            <div class="col-md-11 text-center">
                <input id="btw" name="btw" type="text" placeholder="Btw" class="form-control input-md"
                       required="" value="{{$btw->btw}}">
            </div>

            <div class="col-md-12 text-center">
                <button id="submit" name="submit" class="btn btn-success ">Update</button>
                <a href="/admin/cats_btws/destroyBtw/{{$btw->pk_id_btw}}"><button class="btn btn-danger"><i class = "glyphicon glyphicon-remove"></i></button></a>
                </br></br></br>
            </div>
        </form>

    @endforeach



    <h1>Add new BTW</h1>
    <form method="POST" action="/admin/cats_btws/newBtw" class="form-horizontal"
          enctype="multipart/form-data" role="form">
        {!! csrf_field() !!}
        <label class="col-md-1 control-label text-center" for="btw">Btw</label>
        <div class="col-md-11 text-center">
            <input id="btw" name="btw" type="text" placeholder="Btw" class="form-control input-md" required="">
        </div>

        <div class="col-md-12 text-center">
            <button id="submit" name="submit" class="btn btn-success ">Add</button>
        </div>
    </form>
@endsection