<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\Address;
use App\Http\Requests;

class ClientsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        return view('admin.clients');
    }

    public function showClients(){
        $clients = Client::get();

        return view('admin.clients',['clients' => $clients]);
    }

    //$id = pk_id_client
    public function showClientDetails($id){
        $client = Client::leftjoin('addresses', 'clients.pk_id_client', '=', 'addresses.id_client')
            ->where('clients.pk_id_client', ($id))
            ->get();

        return view('admin.clientDetails',['client' => $client]);
    }

    public function destroy($id){
        Client::destroy($id);

        return redirect('admin/clients');
    }


    //Show update form
    public function update($id) {
        $client = Client::leftjoin('addresses', 'clients.pk_id_client', '=', 'addresses.id_client')
            ->where('clients.pk_id_client', ($id))
            ->get();

        return view('admin.clientUpdate',['client' => $client]);
    }

    //Do update
    public function do_update($id) {
        Client::where('pk_id_client', $id)
            ->update(array(
                'name' => Request::input('name'),
                'email' => Request::input('email'),
                'password' => Request::input('password'),
            ));

        $address = Address::where('id_client', $id)->get();

        if ($address)
        {
            Address::where('id_client', $id)
                ->update(array (
                    'street' => Request::input('street'),
                    'number' => Request::input('number'),
                    'postal_code' => Request::input('postal_code'),
                    'city' => Request::input('city'),
                    'country' => Request::input('country')
                ));
        }
        else
        {
            $newAddress = new Address();
            $newAddress->street = Request::input('street');
            $newAddress->number = Request::input('number');
            $newAddress->postal_code = Request::input('postal_code');
            $newAddress->city = Request::input('city');
            $newAddress->country = Request::input('country');
            $newAddress->id_client = $id;

            $newAddress->save();
        }


        return redirect('admin/client/'.$id);
    }
}

