;(function () {
    'use strict';

    angular.module('app.client').config(Routes);

    /* @ngInject */
    function Routes($stateProvider) {
        $stateProvider
            .state('client', {
                cache: false, // false will reload on every visit.
                controller: 'ClientController as vm',
                templateUrl: 'html/client/client.view.html'
            })
            .state('client.prelogin', {
                cache: false, // false will reload on every visit.
                templateUrl: 'html/client/client.prelogin.view.html',
                url: '/prelogin'
            })
            .state('client.profile', {
                cache: false, // false will reload on every visit.
                templateUrl: 'html/client/client.profile.view.html',
                url: '/profile'
            });
    }

})();