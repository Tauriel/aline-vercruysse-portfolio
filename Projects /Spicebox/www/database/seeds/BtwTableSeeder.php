<?php

use Illuminate\Database\Seeder;
use \App\Models\Btw;

class BtwTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Btw::class, 3)->create();
    }
}
