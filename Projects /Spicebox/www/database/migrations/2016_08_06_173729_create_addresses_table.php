<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('pk_id_address');
            $table->text('street');
            $table->integer('number');
            $table->integer('postal_code');
            $table->text('city');
            $table->text('country');
            $table->integer('id_client')->unsigned();

            $table->foreign('id_client')->references('pk_id_client')->on('clients');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('addresses');
    }
}
