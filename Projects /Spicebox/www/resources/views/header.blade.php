<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="#" class="logo">Spicebox</a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- User Account Menu -->
                <li class="user user-menu">
                    <!-- Menu Toggle Button -->

                    <a href="/auth/logout">
                        Log out
                    </a>


                </li>
            </ul>
        </div>
    </nav>
</header>