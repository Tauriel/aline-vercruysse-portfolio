<?php

use Illuminate\Database\Seeder;
use App\Models\Orderstatus;

class OrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::create([
            'status' => 'Processing'
        ]);
        OrderStatus::create([
            'status' => 'Send'
        ]);
    }
}
