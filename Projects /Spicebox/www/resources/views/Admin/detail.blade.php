@extends('home')

@section('content')
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title">Detail product</div>
            {{--<pre>{{$product}}</pre>--}}
        </div>
        <div class="panel-body detail">
<div class="col-md-10">

</div>
            <div class="col-md-2 text-center">
                <a href="/admin/products/detail/{{$product[0]->pk_id_product}}/update"> <button class="btn btn-success "><i class="glyphicon glyphicon-pencil"></i></button></a>

            </div>

                        <div class="col-md-12 text-center">
                          <img src="{{$product[0]->imgURL}}"/>
                        </div>


                        <label class="col-md-1 control-label text-center" for="name">Name</label>
                        <div class="col-md-11">
                            <input disabled id="name" name="name" type="text" placeholder="Product name" class="form-control input-md" required="" value="{{$product[0]->name}}">
                        </div>

                        <label class="col-md-1 control-label  text-center" for="name">Category</label>
                        <div class="col-md-11">
                            <input disabled id="name" name="name" type="text"  class="form-control input-md" required="" value="{{$product[0]->category_name}}">
                        </div>

                        <label class="col-md-1 control-label text-center" for="name">Price</label>
                        <div class="col-md-11">
                            <input disabled id="name" name="name" type="text" class="form-control input-md" required="" value="€{{$product[0]->price}}">
                        </div>

                        <label class="col-md-1 control-label text-center" for="name">Btw</label>
                        <div class="col-md-11">
                            <input disabled id="name" name="name" type="text"  class="form-control input-md" required="" value="{{$product[0]->btw}}%">
                        </div>

                            <label class="col-md-1 control-label text-center" for="name">Promotion</label>
                            <div class="col-md-11">
                                <textarea disabled>{{$product[0]->promotion}}</textarea >
                            </div>


                        <label class="col-md-1 control-label text-center" for="name">Description</label>
                        <div class="col-md-11">
                            <textarea disabled>{{$product[0]->description}}</textarea >
                        </div>

                        <label class="col-md-1 control-label text-center" for="name">Specifications</label>
                        <div class="col-md-11">
                            <textarea disabled>{{$product[0]->specifications}}</textarea >
                        </div>

                        <label class="col-md-1 control-label text-center" for="name">Rating</label>
                        <div class="col-md-11">
                           <p>

                           </p>

                        </div>

                        <label class="col-md-1 control-label text-center" for="name">Subscription?</label>
                        <div class="col-md-11">
                            <input type="checkbox" name="subscriptionBool" value="{{$product[0]->is_subscription}}">This is a subscription box<br>
                        </div>

                        <label class="col-md-1 control-label text-center" for="name">Created at</label>
                        <div class="col-md-11">
                            <input disabled id="name" name="name" type="text"  class="form-control input-md" required="" value="{{$product[0]->created_at}}">
                        </div>

                        <label class="col-md-1 control-label text-center" for="name">Updated at</label>
                        <div class="col-md-11">
                            <input disabled id="name" name="name" type="text"  class="form-control input-md" required="" value="{{$product[0]->updated_at}}">
                        </div>


        </div>
    </div>
@endsection