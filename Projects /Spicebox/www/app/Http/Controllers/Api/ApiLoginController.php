<?php


namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Client;
use Log;
use App\Http\Controllers\Controller;

class ApiLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('cors');
    }

    //Using angular's $save() method, we POST to this method
    public function store(Request $request)
    {
        $data = $request->all();
        $newClient = new Client();
        $newClient->name = $data['name'];
        $newClient->email = $data['email'];
        $newClient->password = Hash::make($data['password']);
        $newClient->save();

        return $newClient;
    }
}