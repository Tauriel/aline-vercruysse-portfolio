;(function () {
    'use strict';

    angular.module('app.products')
        .controller('ProductsController', ProductsController);

    /* @ngInject */
    function ProductsController($log, $state, ProductResource) {
        $log.info(ProductsController.name, Date.now());
        $log.log('Current state name:', $state.current.name);

        var vm = this;

        vm.$$ui = {
            loader: true
        };

        switch ($state.current.name) {
            case 'products.show': {
                vm.$$ui.title = 'Products';
                vm.products = getProductsAction();
                break;
            }
            default: {
                break;
            }
        }

        function getProductsAction() {
            $log.info(getProductsAction.name);

            vm.$$ui.loader = true;

            return ProductResource.index(success, error);

            function error(error) {
                $log.error(getProductsAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;

                vm.products = [];
            }

            function success(resource, responseHeader) {
                $log.log(getProductsAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                vm.$$ui.loader = false;
            }
        }
    }
})();
