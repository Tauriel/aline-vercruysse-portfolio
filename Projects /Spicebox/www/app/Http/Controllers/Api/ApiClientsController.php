<?php


namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Client;
use App\Models\Address;
use Log;
use App\Http\Controllers\Controller;

class ApiClientsController extends Controller
{
    public function __construct()
    {
        $this->middleware('cors');
    }

    //Using angular's $save() method, POST to this method
    //Due to CORS error popping up every time add a function to this controller
    public function store(Request $request)
    {
        $data = $request->all();

        if($data['clientRequest']) {

            if($data['isNew'])
                return $this->createClient($data);

            return $this->getClient($data);
        }

        if($data['isNew'])
            return $this->createAddress($data);

        $this->updateAddress($data);
    }

    private function createClient($data) {
        $newClient = new Client();
        $newClient->name = $data['name'];
        $newClient->email = $data['email'];
        $newClient->password = $data['password']; //Hash::make password for extra security
        $newClient->save();

        return $newClient;
    }

    private function getClient($data) {
        $client = Client::leftjoin('addresses', 'clients.pk_id_client', '=', 'addresses.id_client')
                    ->where('clients.email', $data['clientemail'])
                    ->where('clients.password', $data['clientpassword']) //Hash::check for security
                    ->firstorfail();

        return $client;
    }

    private function createAddress($data) {

        $newAddress = new Address();
        $newAddress->street = $data['street'];
        $newAddress->number = $data['number'];
        $newAddress->postal_code = $data['postal_code'];
        $newAddress->city = $data['city'];
        $newAddress->country = $data['country'];
        $newAddress->id_client = $data['id_client'];

        $newAddress->save();

        return $newAddress;
    }

    private function updateAddress($data) {
        Address::where('pk_id_address', $data['pk_id_address'])
                    ->update(array(
                        'street' => $data['street'],
                        'number' => $data['number'],
                        'postal_code' => $data['postal_code'],
                        'city' => $data['city'],
                        'country' => $data['country']
                    ));

    }
}

