<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{

    protected $primaryKey = "pk_id_address";

    use SoftDeletes;

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}