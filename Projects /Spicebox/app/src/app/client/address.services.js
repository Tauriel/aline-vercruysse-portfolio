;(function () {
    'use strict';

    angular.module('app.client')
        .factory('AddressResource', AddressResource);

    /* @ngInject */
    function AddressResource($cacheFactory, $resource, UriService) {
        var url = UriService.getApi('/addresses');
        var cache = $cacheFactory('AddressResource');

        var paramDefaults = {};

        var actions = {
            'update': {
                method: 'PUT'
            }

        }

        return $resource(url, paramDefaults, actions);
    }

})();
