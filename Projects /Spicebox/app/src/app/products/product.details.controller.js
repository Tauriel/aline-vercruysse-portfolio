;(function () {
    'use strict';

    angular.module('app.products')
        .controller('ProductDetailsController', ProductDetailsController);


    /* @ngInject */
    function ProductDetailsController($log, $state, ProductDetailResource, LoggedInClientResource, OrderResource) {
        $log.info(ProductDetailsController.name, Date.now());
        $log.log('Current state name:', $state.current.name);

        var vm = this;

        vm.$$ui = {
            loader: true
        };
        vm.$$ix = {};

        switch ($state.current.name) {
            case 'products.details': {
                vm.$$ui.title = 'Product details';
                vm.product = getProductDetailAction();
                vm.clientLoggedIn = (LoggedInClientResource.info.pk_id_client);
                vm.$$ix.save = order;
                break;
            }
            default: {
                break;
            }
        }

        function getProductDetailAction() {
            $log.log(getProductDetailAction.name);

            var params = {
                pk_id_product: $state.params.pk_id_product
            };

            vm.$$ui.loader = true;

            return ProductDetailResource.show(params, success, error);

            function error(error) {
                $log.error(getProductDetailAction.name, 'ERROR', 'error:', error);
                vm.$$ui.loader = false;
            }

            function success(resource, responseHeader) {
                $log.log(getProductDetailAction.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
                vm.product = resource[0];
                vm.$$ui.loader = false;
            }
        }

        function order() {
            $log.log(order.name);

            var neworder = new OrderResource();
            neworder.id_product = $state.params.pk_id_product;
            neworder.id_client = LoggedInClientResource.info.pk_id_client;
            neworder.product_price = vm.product.price;
            neworder.amount = 1;
            neworder.$save(success, error);

            function error(error) {
                $log.error(order.name, 'ERROR', 'error:', error);
            }
            function success(resource, responseHeader) {
                $log.log(order.name, 'SUCCESS', 'resource:', resource, 'responseHeader:', responseHeader());
            }
        }
    }
})();
