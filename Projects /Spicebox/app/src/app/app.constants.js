;(function () {
    'use strict';

    var secure = false;

    angular.module('app')
        .constant('CONFIG', {
            api: {
                protocol: secure ? 'https' : 'http',
                host    : 'www.spicebox.local',
                path    : '/api'
            }
        });
})();