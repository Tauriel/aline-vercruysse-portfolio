<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rating extends Model
{
    protected $primaryKey = "pk_id_rating";
    use SoftDeletes;

    public function client()
    {
        return $this->belongsTo(Client::class);
    }


    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}