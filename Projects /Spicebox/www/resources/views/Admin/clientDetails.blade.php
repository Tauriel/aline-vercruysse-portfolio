@extends('home')

@section('content')

    <div class="row">
        <h1>{{$client[0]->name}}</h1>
        <div class="col-md-12">
            <table class="table table-striped">

                <thead>
                <td>Id</td>
                <td>Name</td>
                <td>Email</td>
                <td>Password</td>
                </thead>

                <tbody>

                <tr>
                    <td>{{$client[0]->pk_id_client}}</td>
                    <td>{{$client[0]->name}}</td>
                    <td>{{$client[0]->email}}</td>
                    <td>{{$client[0]->password}}</td>
                    <td><a href="/admin/client/destroy/{{$client[0]->pk_id_client}}"><button class="btn btn-danger" ><i class = "glyphicon glyphicon-remove"></i></button></a>
                        <a href="/admin/client/{{$client[0]->pk_id_client}}/update"> <button class="btn btn-success "><i class="glyphicon glyphicon-pencil"></i></button></a></td>
                </tr>
                </tbody>
            </table>


            <table class="table table-striped">
                <h2>Address</h2>
                <thead>
                    <td>Street</td>
                    <td>Postal code</td>
                    <td>City</td>
                    <td>Country</td>
                </thead>
                <tbody>
                    <tr>
                        <td>
                        <td>{{$client[0]->street}}</td>
                        <td>{{$client[0]->postal_code}}</td>
                        <td>{{$client[0]->city}}</td>
                        <td>{{$client[0]->country}}</td>
                    </tr>
                </tbody>
            </table>
                </div>
            </div>
@endsection