<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    const AMOUNT = [
        'NONE' => 0,
        'MIN' => 1,
        'FEW' => 3,
        'SOME' => 5,
        'DEFAULT' => 10,
        'MANY' => 100,
        'MAX' => 1000,
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        //disable foreign key check for this connection before truncating -> so we don't get foreign key constraint errors
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Clear all table data before seeding (except migrations table)
        $database = DB::select('SELECT DATABASE() AS name');
        $col = 'Tables_in_' . $database[0]->name;
        $tables = array_except(DB::select('SHOW TABLES'), ['migrations']);

        foreach ($tables as $table) {
            DB::table($table->$col)->truncate();
        }

        //reset foreign key check
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // Seeder order is important for database relations!
        $seeders = [
            UsersTableSeeder::class,
            BtwTableSeeder::class,
            CategoryTableSeeder::class,
            ProductsTableSeeder::class,
            OrderStatusTableSeeder::class,
        ];

        $i = 0;
        foreach ($seeders as $seeder) {
            $count = sprintf('%02d', ++$i);
            $this->command->getOutput()->writeln("<comment>Seed${count}:</comment> ${seeder}...");
            $this->call($seeder);
        }
    }
}
