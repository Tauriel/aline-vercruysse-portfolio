<?php


namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Address;
use Log;
use App\Http\Controllers\Controller;


//to avoid CORS issues -> seperate controller
class ApiAddressesController extends Controller
{
    public function __construct()
    {
        $this->middleware('cors');
    }

    //Using angular's $save() method, POST to this method
    public function store(Request $request)
    {
            $data = $request->all();

            $newAddress = new Address();
            $newAddress->street = $data['street'];
            $newAddress->number = $data['number'];
            $newAddress->postal_code = $data['postal_code'];
            $newAddress->city = $data['city'];
            $newAddress->country = $data['country'];
            $newAddress->id_client = $data['id_client'];

            $newAddress->save();

            return $newAddress;

    }

    public function update(Request $request)
    {
        $data = $request->all();

        //get info from frontend through request, but in $data
        $address = Address::where('addresses.pk_id_address', $data['pk_id_address'])->firstorfail();
        $address->street = $data['street'];
        $address->number = $data['number'];
        $address->postal_code = $data['postal_code'];
        $address->city = $data['city'];
        $address->country = $data['country'];

        $address->save();

        return $address;
    }
}