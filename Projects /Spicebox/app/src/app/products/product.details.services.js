;(function () {
    'use strict';

    angular.module('app.products')
        .factory('ProductDetailResource', ProductDetailResource);

    /* @ngInject */
    function ProductDetailResource($cacheFactory, $resource, UriService) {
        var url = UriService.getApi('/products/:pk_id_product');
        var cache = $cacheFactory('ProductDetailResource');

        var paramDefaults = {
            pk_id_product: '@pk_id_product' // @id is the property, e.g. `post.id` if the resource instance is `post`.
        };

        var actions = {
            'show': {
                cache: cache,
                method: 'GET',
                isArray: true
            }
        };

        return $resource(url, paramDefaults, actions);
    }
}());