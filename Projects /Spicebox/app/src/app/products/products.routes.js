//;(function () {
//    'use strict';
//
//    angular.module('app.products').config(Routes);
//
//    /* @ngInject */
//    function Routes($stateProvider) {
//        $stateProvider
//            .state('products', {
//                cache: false, // false will reload on every visit.
//                controller: 'ProductsController as prodCtrl',
//                templateUrl: 'html/products/products.view.html',
//                url: '/products'
//            });
//    }
//
//})();

;(function () {
    'use strict';

    angular.module('app.products')
        .config(Routes);

    /* @ngInject */
    function Routes($stateProvider) {
        $stateProvider
            .state('products', {
                cache: false, // false will reload on every visit.
                controller: 'ProductsController as vm',
                templateUrl: 'html/products/products.view.html'
            })
            .state('products.show', {
                cache: false, // false will reload on every visit.
                templateUrl: 'html/products/products.show.view.html',
                url: '/products'
            })
            .state('products.details', {
                cache: false, // false will reload on every visit.
                controller: 'ProductDetailsController as vm',
                templateUrl: 'html/products/product.details.view.html',
                url: '/products/details/{pk_id_product:int}'
            });
    }

})();