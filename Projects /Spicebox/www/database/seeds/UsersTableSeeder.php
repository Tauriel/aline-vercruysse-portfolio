<?php

use Illuminate\Database\Seeder;
use \Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'name' => 'root',
            'email' => 'root@root.root',
            'password' => Hash::make('root'),
            'given_name' => 'Root',
            'family_name' => 'Rootingson'
        ]);
    }
}