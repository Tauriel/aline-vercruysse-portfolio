<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    protected $primaryKey = "pk_id_product";
    use SoftDeletes;

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function btw()
    {
        return $this->belongsTo(Btw::class);
    }

    public function orders (){
        return $this -> HasMany(Order::class);
    }


}
