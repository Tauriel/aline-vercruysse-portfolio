@extends('home')

@section('content')

 <div class="row">
    <h1>Products</h1>
    <div class="col-md-12">
        <table class="table table-striped">

            <thead>
                <td>Id</td>
                <td>Name</td>
                <td>Price</td>
                <td>BTW</td>
                <td>Promotion</td>
                <td>Category</td>
            </thead>
            <tbody>
        @foreach ($products as $product)
                <tr>
                    <td>{{$product->pk_id_product}}</td>
                    <td>{{$product->name}}</td>
                    <td>€{{$product->price}}</td>
                    <td>{{$product->btw}}%</td>
                    <td>{{$product->promotion_description}}</td>
                    <td>{{$product->category_name}}</td>
                    <td>{{$product->amount_sold}}</td>
                    <td>
                        <a href="/admin/products/destroy/{{$product->pk_id_product}}"><button class="btn btn-danger"><i class = "glyphicon glyphicon-remove"></i></button></a>
                        <a href="/admin/products/detail/{{$product->pk_id_product}}/update"> <button class="btn btn-success "><i class="glyphicon glyphicon-pencil"></i></button></a>
                        <a href="/admin/products/detail/{{$product->pk_id_product}}"><button class="btn btn-info"><i class="glyphicon glyphicon-eye-open"></i> </button></a>
                    </td>
                </tr>
           </tbody>
    @endforeach

        </table>
    </div>
 </div>
@endsection