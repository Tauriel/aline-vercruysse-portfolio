;(function () {
    'use strict';

    angular.module('app.products')
        .factory('OrderResource', OrderResource);

    /* @ngInject */
    function OrderResource($cacheFactory, $resource, UriService) {
        var url = UriService.getApi('/orders/:pk_id_client');
        var cache = $cacheFactory('OrderResource');

        var paramDefaults = {
            pk_id_product: '@pk_id_product', // @id is the property, e.g. `post.id` if the resource instance is `post`.
            pk_id_client: '@pk_id_client'
        };

        var actions = {
            'show': {
                cache: cache,
                method: 'GET',
                isArray: true
            }
        };

        return $resource(url, paramDefaults, actions);
    }
}());