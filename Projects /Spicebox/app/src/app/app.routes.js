;(function () {
    'use strict';

    angular.module('app').config(Routes);

    /* @ngInject */
    function Routes($urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
    }

})();