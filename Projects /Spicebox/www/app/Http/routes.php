<?php
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
*/

Route::auth();
Route::get('auth/logout', 'Auth\AuthController@getLogout');

//ADMINS ONLY
//============
Route::group(['middleware' => ['adminOnly'], 'prefix' => 'admin'], function () {

    Route::get('dashboard', 'DashboardController@index');

    Route::group(['prefix' => 'products'], function() {
        //Product routes
        Route::get('', 'ProductsController@showProds');
        Route::get('detail/{id}', 'ProductsController@detail');
        Route::get('new', 'ProductsController@newProduct');
        Route::post('save', 'ProductsController@add');
        Route::get('detail/{id}/update', 'ProductsController@update');
        Route::get('destroy/{id}', 'ProductsController@destroy');
        Route::post('{id}/do_update', 'ProductsController@do_update');
    });


    //Categories and Btw routes
    Route::group(['prefix' => 'cats_btws'], function () {
        Route::get('', 'CatBtwController@showCatsBtws');
        Route::post('newCat', 'CatBtwController@addCategory');
        Route::get('destroyCat/{id}', 'CatBtwController@destroyCat');
        Route::post('doBtwUpdate/{id}', 'CatBtwController@doCatUpdate');
        Route::post('newBtw', 'CatBtwController@addBtw');
        Route::get('destroyBtw/{id}', 'CatBtwController@destroyBtw');
        Route::post('doBtwUpdate/{id}', 'CatBtwController@doBtwUpdate');
    });

    Route::get('orders', 'OrdersController@index');
    Route::get('orders', 'OrdersController@showOrders');
    Route::get('orders/destroy/{id}', 'OrdersController@destroyOrder');


    //Clients
    Route::get('clients', 'ClientsController@showClients');
    Route::get('client/{id}', 'ClientsController@showClientDetails');
    Route::get('client/destroy/{id}', 'ClientsController@destroy');
    Route::get('client/{id}/update', 'ClientsController@update');
    Route::post('client/{id}/do_update', 'ClientsController@do_update');
});

require_once 'routesApi.php';