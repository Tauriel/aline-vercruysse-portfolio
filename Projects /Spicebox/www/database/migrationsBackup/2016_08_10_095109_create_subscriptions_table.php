<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{

    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('pk_id_subscription');
            $table->integer('id_product')->unsinged();
            $table->integer('id_client')->unsinged();

            $table->foreign('id_product')->references('pk_id_product')->on('products');
            $table->foreign('id_client')->references('pk_id_client')->on('clients');

            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::drop('subscriptions');
    }
}
