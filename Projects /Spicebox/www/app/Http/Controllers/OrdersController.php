<?php

    namespace App\Http\Controllers;

use Request;
use App\Models\Client;
use App\Models\Order;
use App\Http\Requests;

class OrdersController extends Controller
{
        public function __construct()
    {
        $this->middleware('auth');
    }

        public function index(){

        return view('admin.orders');
    }

        public function showOrders(){
        $orders = Order::join('clients', 'orders.id_client', '=', 'clients.pk_id_client')
            ->join('order_statuses', 'orders.id_order_status', '=', 'order_statuses.pk_id_order_status')
            ->get();

        return view('admin.orders',['orders' => $orders]);
    }

    public function destroyOrder($id){
        Order::destroy($id);

        return redirect('admin/orders');
    }

}


