;(function () {
    'use strict';

    angular.module('app.howItWorks')
        .directive('howItWorks', function() {

            return {
                restrict: 'E',
                templateUrl: 'html/howItWorks/howItWorks.view.html'
            };
        });

})();
