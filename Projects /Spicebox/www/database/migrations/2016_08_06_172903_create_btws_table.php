<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBtwsTable extends Migration
{

    public function up()
    {
        Schema::create('btws', function (Blueprint $table) {
            $table->increments('pk_id_btw');
            $table->integer('btw');

            $table->timestamps();
            $table->softDeletes();
        });
    }


    public function down()
    {
        Schema::drop('btws');
    }
}
