<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{

    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('pk_id_order');
            $table->integer('id_product');
            $table->integer('product_price');
            $table->integer('amount');
            $table->integer('id_order_status')->unsigned();
            $table->integer('id_client')->unsigned();

            $table->foreign('id_order_status')->references('pk_id_order_status')->on('order_statuses')->unsigned();
            $table->foreign('id_client')->references('pk_id_client')->on('clients')->unsigned();
            //$table->foreign('id_product')->references('pk_id_product')->on('products');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('orders');
    }
}
