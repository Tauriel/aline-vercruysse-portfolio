<?php

use App\User;
use App\Models\Category;
use App\Models\Btw;
use App\Models\Product;
use App\Models\Address;
use App\Models\Client;
use App\Models\Order;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Category::class, function (Faker $faker) : array {
    return [
        'category_name' => $faker->unique()->word(),
        'category_description' => $faker->paragraph($nbSentences = 3)
    ];
});

$factory->define(Btw::class, function (Faker $faker) : array {
    return [
        'btw' => $faker->numberBetween(1, 100)
    ];
});

$factory->define(User::class, function (Faker $faker) : array {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => Hash::make($faker->password($minLength = 6, $maxLength = 20)),
        'remember_token' => str_random(10),
        'given_name' => $faker->firstName,
        'family_name' => $faker->lastName,
    ];
});

$factory->define(Product::class, function (Faker $faker) : array {
    return [
        'name' => $faker->name,
        'imgURL' => $faker->imageUrl(),
        'description' => $faker->text(10),
        'specifications' => $faker->text(10),
        'price' => $faker->numberBetween(1, 100),
        'promotion' => $faker->numberBetween(1, 100),
        'id_category' => $faker->numberBetween(1, 5),
        'id_btw' => $faker->numberBetween(1, 3)
    ];
});

$factory->define(Address::class, function (Faker $faker) : array {
    return [
        'street' => $faker->streetName,
        'number' => $faker->numberBetween(1, 100),
        'postal_code' => $faker->postcode,
        'city' => $faker->city,
        'country' => $faker->country,
        'id_client' => $faker->numberBetween(1, 10)
    ];
});

$factory->define(Client::class, function (Faker $faker) : array {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->email,
        'password' => $faker->password($minLength = 6, $maxLength = 20)
    ];
});

$factory->define(Order::class, function (Faker $faker) : array {
    return [
        'id_product' => $faker->numberBetween(1,10),
        'product_price' => $faker->numberBetween(1,100),
        'amount' => $faker->numberBetween(1,10),
        'id_client' => $faker->numberBetween(1, 10)
    ];
});
