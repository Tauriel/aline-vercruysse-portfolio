

@extends('home')
@section('content')
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title">Update product (ID: {{$product[0]->pk_id_product}})</div>
        </div>
        <div class="panel-body detail">
            <form method="POST" action="/admin/products/{{$product[0]->pk_id_product}}/do_update" class="form-horizontal"
                  enctype="multipart/form-data" role="form">
                {!! csrf_field() !!}
                <label class="col-md-1 control-label text-center" for="imgURL">Image</label>
                <div class="col-md-11 text-center">
                    <input id="imgURL" name="imgURL" type="text" class="form-control input-md"
                           value="{{$product[0]->imgURL}}"/>
                </div>

                <label class="col-md-1 control-label text-center" for="name">Name</label>
                <div class="col-md-11">
                    <input id="name" name="name" type="text" placeholder="Product name" class="form-control input-md"
                           required="" value="{{$product[0]->name}}">
                </div>

                <label class="col-md-1 control-label text-center" for="category">Category</label>
                <div class="col-md-11">
                    <select class="form-control" name="category">
                        @foreach ($categories as $category)
                            @if ($category->pk_id_category == $product[0]->id_category)
                                <option id="{{$category->category_name}}" value="{{$category->pk_id_category}}" selected="selected">{{$category->category_name}}</option>
                            @else
                                <option id="{{$category->category_name}}" value="{{$category->pk_id_category}}">{{$category->category_name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <label class="col-md-1 control-label text-center" for="name">Price</label>
                <div class="col-md-11">
                    <input id="price" name="price" type="number" class="form-control input-md" required="">
                </div>

                <label class="col-md-1 control-label text-center" for="btw">Btw</label>
                <div class="col-md-11">
                    <select class="form-control" name="btw">
                        @foreach ($btws as $btw)
                            @if ($category->pk_id_btw == $product[0]->id_btw)
                                <option id="{{$btw->btw}}" value="{{$btw->pk_id_btw}}" selected="selected">{{$btw->btw}}%</option>
                            @else
                                <option id="{{$btw->btw}}" value="{{$btw->pk_id_btw}}">{{$btw->btw}}%</option>
                            @endif
                        @endforeach
                    </select>
                </div>


                <label class="col-md-1 control-label text-center" for="promotion">Promotion</label>
                <div class="col-md-11">
                    <input id="promotion" name="promotion" type="text" placeholder="Product promotion" class="form-control input-md"
                           required="" value="{{$product[0]->promotion}}">
                </div>

                <label class="col-md-1 control-label text-center" for="name">Description</label>
                <div class="col-md-11">
                    <textarea class="form-control" name="description">{{$product[0]->description}}</textarea>
                </div>

                <label class="col-md-1 control-label text-center" for="name">Specifications</label>
                <div class="col-md-11">
                    <textarea class="form-control" name="specifications">{{$product[0]->specifications}}</textarea>
                </div>



                <div class="col-md-12 text-center">
                    <button id="submit" name="submit" class="btn btn-success ">Update</button>
                </div>
            </form>
        </div>
    </div>
    <script>
        document.getElementById("price").value = "{{$product[0]->price}}";
        document.getElementById({{$product[0]->btw}}).selected = true;
        document.getElementById("{{$product[0]->categorie}}").selected = true;
        document.getElementById("{{$product[0]->promotion_description}}").selected = true;


    </script>
@endsection