<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
    protected $primaryKey = "pk_id_subscription";
    use SoftDeletes;

    public function client()
    {
        return $this->belongsTo(Client::class);
    }


    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}