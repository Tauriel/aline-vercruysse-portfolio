<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{

    public function up()
    {

        Schema::create('products', function (Blueprint $table) {
            $table->increments('pk_id_product');

            $table->string('name')->unique();
            $table->string('imgURL');
            $table->text('description');
            $table->text('specifications');
            $table->integer('price')->nullable();
            $table->integer('promotion')->unsigned()->nullable();
            $table->boolean('is_subscription');
            $table->integer('id_category')->unsigned()->nullable();
            $table->integer('id_btw')->unsigned()->nullable();

            $table->foreign('id_category')->references('pk_id_category')->on('categories');
            $table->foreign('id_btw')->references('pk_id_btw')->on('btws');


            $table->timestamps();
            $table->softDeletes();

        });
    }


    public function down()
    {
        Schema::drop('id');
    }
}
