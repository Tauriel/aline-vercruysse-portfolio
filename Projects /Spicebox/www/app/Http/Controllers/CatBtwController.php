<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Requests;
use App\Models\Category;
use App\Models\Btw;

class CatBtwController extends Controller
{

    public function showCatsBtws(){
        $categories = Category::get();
        $btws = Btw::get();
        return view('admin.catBtw',['categories' => $categories, 'btws' => $btws]);
    }

    public function addCategory () {
        $category  = new Category();
        $category->category_name = Request::input('name');
        $category->category_description =Request::input('desc');

        $category->save();

        return redirect('/admin/cats_btws');
    }

    public function doCatUpdate($id) {
        Category::where('pk_id_category', $id)
            ->update(array(
                'category_name' => Request::input('name'),
                'category_description' => Request::input('desc'),
            ));

        return redirect('/admin/cats_btws');
    }

    public function destroyCat($id) {
        Category::destroy($id);

        return redirect('/admin/cats_btws');
    }

    //
    //BTW
    //
    public function addBtw () {
        $btw  = new Btw();
        $btw->btw = Request::input('btw');

        $btw->save();

        return redirect('/admin/cats_btws');
    }

    public function doBtwUpdate($id) {
        Btw::where('pk_id_btw', $id)
            ->update(array(
                'btw' => Request::input('btw'),
            ));

        return redirect('/admin/cats_btws');
    }

    public function destroyBtw($id) {
        Btw::destroy($id);

        return redirect('/admin/cats_btws');
    }

}
