;(function () {
    'use strict';

    //add factory LoggedInClientResource to store info of logged in user
    angular.module('app')
        .factory('LoggedInClientResource', LoggedInClientResource);

    /* @ngInject */
    function LoggedInClientResource() {
        var client = {};

        client.info = {};
        client.address = {};

        return client;
    }

})();





