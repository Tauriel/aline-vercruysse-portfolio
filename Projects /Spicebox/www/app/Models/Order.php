<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{

    protected $primaryKey = "pk_id_order";
    use SoftDeletes;

    protected $fillable = [
        'id_product'
    ];

    public function orderStatus()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}