# Aline Vercruysse - Web developer

## Mercury - carpool webapp

![mecurylogo.png](https://s12.postimg.org/u9swioxq5/mecurylogo.png)

[![mercury1.png](https://s21.postimg.org/8gj8j1olj/mercury1.png)](https://postimg.org/image/5z7hbs4oz/)

[![mercury2.png](https://s10.postimg.org/e74872t2h/mercury2.png)](https://postimg.org/image/mpdobezl1/)

[![mercury3.png](https://s11.postimg.org/w7m3vq1df/mercury3.png)](https://postimg.org/image/j3gjj19bj/)

## Spicebox - Eshop

[![spicebox-icons.png](https://s15.postimg.org/3lq6qe0ln/spicebox_icons.png)](https://postimg.org/image/ovdt18gw7/)

[![Screen Shot 2016-08-16 at 3.17.54 PM.png](https://s21.postimg.org/3w2cavz3r/Screen_Shot_2016_08_16_at_3_17_54_PM.png)](https://postimg.org/image/zfsdkz59v/)

[![Screen Shot 2016-08-16 at 3.17.24 PM.png](https://s17.postimg.org/59ovrq4hr/Screen_Shot_2016_08_16_at_3_17_24_PM.png)](https://postimg.org/image/tq71m758b/)

[![Screen Shot 2016-08-16 at 3.16.52 PM.png](https://s10.postimg.org/82l4b4k55/Screen_Shot_2016_08_16_at_3_16_52_PM.png)](https://postimg.org/image/lw9h06cqd/)

[![Screen Shot 2016-08-16 at 3.16.26 PM.png](https://s15.postimg.org/9dqg24qjf/Screen_Shot_2016_08_16_at_3_16_26_PM.png)](https://postimg.org/image/5u4icbntj/)

[![Screen Shot 2016-08-16 at 3.17.04 PM.png](https://s15.postimg.org/ll4qx8d63/Screen_Shot_2016_08_16_at_3_17_04_PM.png)](https://postimg.org/image/rm2fuazs7/)


## MOM - Medical Overview Manager - Icons and logo
###Project by Frederik Lefevre
http://www.frederiklefevre.com/project.php?projectID=6

[![icons.png](https://s3.postimg.org/bp7ekv377/icons.png)](https://postimg.org/image/3jpcmpey7/)

## Contact

alinverc@student.arteveldehs.be
