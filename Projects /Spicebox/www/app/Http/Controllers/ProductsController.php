<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Requests;
use App\Models\Product;
use App\Models\Category;
use App\Models\Btw;
use Log;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.products');
    }

    public function showProds(){
        $products = Product::join('btws', 'products.id_btw', '=', 'btws.pk_id_btw')
            ->join('categories', 'products.id_category', '=', 'categories.pk_id_category')
            ->get();

        //$products = Product::get();

        return view('admin.products',['products' => $products]);
    }

    //Show form to add new product
    public function newProduct(){
        $categories = Category::get();
        $btws = Btw::get();
        return view('admin.new', ['categories' => $categories, 'btws' => $btws]);
    }

    //Add a new product
    public function add() {
        $product  = new Product();
        $product->name = Request::input('name');
        $product->description =Request::input('description');
        $product->specifications =Request::input('specifications');
        $product->price =Request::input('price');
        $product->imgURL =Request::input('imageurl');
        $product->id_category = Request::input('category');
        $product->promotion = Request::input('promotion');
        $product->id_btw = Request::input('btw');
        $product->is_subscription = false;

        $product->save();

        return redirect('/admin/products');
    }

    public function detail($id) {
        $product = Product::join('btws', 'products.id_btw', '=', 'btws.pk_id_btw')
            ->join('categories', 'products.id_category', '=', 'categories.pk_id_category')
            ->where('products.pk_id_product', ($id))
            ->get();

        return view('admin.detail',['product' => $product]);
    }


    //Show update form
    public function update($id) {
        $product = Product::join('btws', 'products.id_btw', '=', 'btws.pk_id_btw')
            ->join('categories', 'products.id_category', '=', 'categories.pk_id_category')
            ->where('products.pk_id_product', ($id))
            ->get();

        $categories = Category::get();
        $btws = Btw::get();

        return view('admin.update',['product' => $product, 'categories' => $categories, 'btws' => $btws]);
    }

    //Do update
    public function do_update($id) {
        Product::where('pk_id_product', $id)
            ->update(array(
                'name' => Request::input('name'),
                'price' => Request::input('price'),
                'imgURL' => Request::input('imgURL'),
                'id_category' => Request::input('category'),
                'id_btw' => Request::input('btw'),
                'promotion' => Request::input('promotion'),
                'description' => Request::input('description'),
                'specifications' => Request::input('specifications'),
            ));

        return redirect('/admin/products');
    }

    public function destroy($id){
        Product::destroy($id);

        return redirect('admin/products');
    }
}

