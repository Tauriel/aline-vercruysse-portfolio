;(function () {
    'use strict';

    angular.module('app')
        .controller('NavController', NavController);

    /* @ngInject */
    function NavController($window) {

        //Variables
        this.currentPage = 'Home';
        //title decides what to show as subtitle
        this.title = 'Spice up your life \r\n with our awesome Spiceboxes!';

        //Remember last navigated page and title (used when the user decides to login/register)
        this.previousPage = 'Home';
        this.previousTitle = 'Spice up your life \r\n with our awesome Spiceboxes!';

        this.HOMEID = 'Home';
        this.PRODUCTSID = 'Products';
        this.HOWID = 'How';
        this.JOINID = 'Register';
        this.PROFILEID = 'Profile';
        this.CARTID = 'Cart';
        this.SUBSCRIPTIONID = 'Subscription';
        this.ACCESSORYID = 'Accessory';

        this.subscriptionDetail = undefined;
        this.accessoryDetail = undefined;

        //Functions
        //==========
        //Functions below are used to set specific content visible (active)
        //Each function will also set the correct subtitle for that content
        this.setHomePageActive = function () {
            this.setPageActive(this.HOMEID, 'Spice up your life \r\n with our awesome Spiceboxes!', '/#/');
        }
        this.setProductsPageActive = function () {
            this.setPageActive(this.PRODUCTSID, 'Our Products', '/#/products');
        }
        this.setHowPageActive = function () {
            this.setPageActive(this.HOWID, 'How it works', '/#/howItWorks');
        }
        this.setJoinPageActive = function () {
            this.setPageActive(this.JOINID, 'Register', '/#/prelogin');
        }

        this.setSubscriptionPageActive = function (subscription) {
            if (subscription != undefined) {
                this.subscriptionDetail = subscription;
                this.setPageActive(this.SUBSCRIPTIONID, subscription.name);
            }
        }
        this.setAccessoryPageActive = function (accessory) {
            if (accessory != undefined) {
                this.accessoryDetail = accessory;
                this.setPageActive(this.ACCESSORYID, accessory.name);
            }
        }

        this.setLastPageActive = function() {
            this.setPageActive(this.previousPage, this.previousTitle);
        }

        //This is the function to change the subtitle and current page
        this.setPageActive = function(page, title, url) {
            this.previousPage = this.currentPage;
            this.currentPage = page;
            this.previousTitle = this.title;
            this.title = title;

            if (this.currentPage !== this.SUBSCRIPTIONID)
                this.subscriptionDetail = undefined;
            if (this.currentPage !== this.ACCESSORYID)
                this.accessoryDetail = undefined;

            $window.location.href = url;
        }

        this.isCurrentlyHomePage = function () {
            return this.currentPage === this.HOMEID;
        }
        this.isCurrentlyProductsPage = function () {
            return this.currentPage === this.PRODUCTSID;
        }
        this.isCurrentlyHowPage = function () {
            return this.currentPage === this.HOWID;
        }
        this.isCurrentlyJoinPage = function () {
            return this.currentPage === this.JOINID;
        }
        this.isCurrentlyProfilePage = function () {
            return this.currentPage === this.PROFILEID;
        }
        this.isCurrentlyCartPage = function () {
            return this.currentPage === this.CARTID;
        }
        this.isSubscriptionPageActive = function () {
            return this.currentPage === this.SUBSCRIPTIONID;
        }
        this.isAccessoryPageActive = function () {
            return this.currentPage === this.ACCESSORYID;
        }

        var originatorEv;

        this.openMenu = function ($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        };
    }

})();