@extends('home')

@section('content')

    <div class="row">
        <h1>Orders</h1>
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <td>Order id</td>
                <td>Client</td>
                <td>Product price</td>
                <td>Amount</td>
                <td>Order status</td>
                <td>Date placed</td>
                </thead>
                <tbody>
                @foreach($orders as $order)

                    <tr>
                        <td>{{$order->pk_id_order}}</td>
                        <td>{{$order->name}}</td>
                        <td>{{$order->product_price}}</td>
                        <td>{{$order->amount}}</td>
                        <td>{{$order->order_status}}</td>
                        <td>{{$order->created_at}}</td>
                        <td>
                            <a href="/admin/orders/destroy/{{$order->pk_id_order}}"><button class="btn btn-danger" ><i class = "glyphicon glyphicon-remove"></i></button></a>
                        </td>
                    </tr>

                @endforeach
                </tbody>

            </table>
        </div>
    </div>
@endsection










