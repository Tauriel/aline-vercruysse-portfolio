<?php


namespace App\Http\Controllers\Api;

use App\Models\Order;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;


//to avoid CORS issues -> seperate controller
class ApiOrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('cors');
    }

    Public function index(){
        return "index";
    }

    //Using angular's $save() method, POST to this method
    public function store(Request $request)
    {
        $data = $request->all();

        $newOrder = new Order();
        $newOrder->id_product = $data['id_product'];
        $newOrder->product_price = $data['product_price'];
        $newOrder->amount = $data['amount'];
        $newOrder->id_order_status = 1;
        $newOrder->id_client = $data['id_client'];
        $newOrder->save();

        return $newOrder;
    }


    public function show (Request $request){
        $data = $request->all();
        return $data['pk_id_client'];
    }
}