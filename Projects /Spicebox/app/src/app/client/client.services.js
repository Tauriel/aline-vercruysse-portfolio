;(function () {
    'use strict';

    angular.module('app.client')
        .factory('ClientResource', ClientResource);

    /* @ngInject */
    function ClientResource($cacheFactory, $resource, UriService) {
        var url = UriService.getApi('/clients');
        var cache = $cacheFactory('ClientResource');

        var paramDefaults = {
            action : '@action'
        };

        var actions = {
            'show': {
                cache: cache,
                method: 'GET',
                isArray: true
            }
        };

        return $resource(url, paramDefaults, actions);
    }

})();
