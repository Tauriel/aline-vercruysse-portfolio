@extends('layouts/app')

@section('content')

        <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>dashboard</h1>

    </section>
    <!-- Main content -->
    <section class="content">

        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>products</h3>
                            <p>Producten</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-leaf"></i>
                        </div>

                    </div>
                </div><!-- ./col -->
                <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>klanten</h3>
                            <p>Klanten</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>

                    </div>
                </div><!-- ./col -->

            </div><!-- /.row -->
            <!-- Main row -->
            <div class="row">

                <!-- right col (We are only adding the ID to make the widgets sortable)-->
                <section class="col-lg-5 connectedSortable">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Best selling products</h3>
                            <div class="box-tools pull-right">
                                <button class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">




                        </div>
                        <div>

                        </div>
                    </div>
                </section>
                <section class="col-lg-5 connectedSortable">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Best Clients</h3>
                            <div class="box-tools pull-right">
                                <button class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <button class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <table class="table table-striped">

                            </table>

                        </div>
                        <div>

                        </div>
                    </div>
                </section>
            </div><!-- /.row (main row) -->

        </section><!-- /.content -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

@stop
