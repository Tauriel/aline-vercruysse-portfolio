<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration
{

    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('pk_id_ratings');
            $table->integer('rating');
            $table->integer('id_client')->unsigned();
            $table->integer('id_product')->unsigned();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('ratings', function(Blueprint $table) {
            $table->foreign('id_client')->references('pk_id_client')->on('clients');
            $table->foreign('id_product')->references('pk_id_product')->on('products');
        });
    }

    public function down()
    {
        Schema::drop('ratings');
    }
}
