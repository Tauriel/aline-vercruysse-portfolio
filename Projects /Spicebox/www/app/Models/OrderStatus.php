<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orderstatus extends Model
{
    protected $primaryKey = "pk_id_order_status";
    use SoftDeletes;

    protected $table = 'order_statuses';

    public function orders (){
        return $this -> hasMany(Order::class);

    }
}