//connect to backoffice

//;(function () {
//    'use strict';
//
//    angular.module('app.products')
//        .factory('ProductResource', ProductResource);
//
//    /* @ngInject */
//    function ProductResource($resource, UriService) {
//        console.log(url);
//        var url = UriService.getApi('/products');
//
//
//        var paramDefaults = {};
//
//        //https://laravel.com/docs/5.1/controllers
//        //define actions from routes
//        var actions = {
//            'index': {
//                method: 'GET'
//            }
//        };
//
//        return $resource(url, paramDefaults, actions);
//    }
//
//})();

;(function () {
    'use strict';

    angular.module('app.products')
        .factory('ProductResource', ProductResource);

    /* @ngInject */
    function ProductResource($cacheFactory, $resource, UriService) {
        var url = UriService.getApi('/products');
        var cache = $cacheFactory('ProductResource');

        var paramDefaults = {};

        var actions = {
            'index': {
                cache: cache,
                method: 'GET',
                isArray: true
            }
        };

        return $resource(url, paramDefaults, actions);
    }
})();
